
"""
---------- Dedicated to Mary our mother in spirit, and to her son Jesus our brother in spirit.
---------- Just like Adam and Eve. A new beginning...
"""

import sys
sys.path.append('./libs')
import xml2gui as g
import webbrowser


def listener_click(id, msg):
    if msg == 'up':
        if id == 'image1':
            webbrowser.open("https://gitlab.com/simple-gui/xml2gui-adverts")


if __name__ == '__main__':
    xml = open('app.xml', mode='r', encoding='utf-8').read()
    g.mcxml_listener_click(listener_click)
    g.mcxml_loop(xml)
    
