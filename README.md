# 🎄

## Adverts

### Dedicated to Our Lady of La Salette :one:

Built with xml2gui Framework (C/C++, C#, Java/Kotlin, Python).  

Run with Python 3.6 and above (64 bit).

![Screenshot 0](https://gitlab.com/simple-gui/xml2gui-adverts/-/raw/main/images/sh0.png)  
![Screenshot 1](https://gitlab.com/simple-gui/xml2gui-adverts/-/raw/main/images/any_help.gif)
![Ad 2](https://gitlab.com/simple-gui/xml2gui-adverts/-/raw/main/images/a2.png)
